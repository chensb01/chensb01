/* SPIDER -- a sample adventure game, by David Matuszek.
   Consult this file and issue the command "start."  */

:- dynamic at/2, whereyouare/1, alive/1.   /* Needed by SWI-Prolog. */

/* This defines my current location. */

whereyouare(meadow).

/* Facts describe how the rooms are connected. 
Facts detailing locations */
		
walk(troll, d, bridge).
walk(bridge, u, troll).

walk(bridge, w, bridge_entrance).
walk(bridge_entrance, e, bridge).

walk(bridge_entrance, s, meadow).
walk(meadow, n, bridge_entrance) :- at(flashlight, in_hand).
walk(meadow, n, bridge_entrance) :-
        write('go into that dark cave without a light?  are you crazy?'), nl,
        fail.

walk(meadow, s, building).
walk(building, n, meadow).

walk(building, w, cage).
walk(cage, e, building).

walk(closet, w, building).
walk(building, e, closet) :- at(key, in_hand).
walk(building, e, closet) :-
        write('the door appears to be locked.'), nl,
        fail.
		
		
		

/* These facts tell where the various objects in the game
   are located. */

at(pieceofgold, troll).
at(key, bridge_entrance).
at(flashlight, building).
at(sword, closet).


/* This fact specifies that the spider is alive. */

take(X) :-
        at(X, in_hand),
        write('You''re already holding it!'),
        nl.

take(X) :-
        whereyouare(Setting),
        at(X, Setting),
        retract(at(X, Setting)),
        assert(at(X, in_hand)),
        write('OK.'),
        nl.
		
take(_) :-
        write('I don''t see it here.'),
        nl.


/* These rules describe how to put down an object. */
drop(X) :-
        at(X, in_hand),
        whereyouare(Setting),
        retract(at(X, in_hand)),
        assert(at(X, Setting)),
        write('OK.'),
        nl.

drop(_) :-
        write('You aren''t holding it!'),
        nl.


/* These rules define the six direction letters as calls to go/1. */

n :- go(n).

s :- go(s).

e :- go(e).

w :- go(w).

u :- go(u).

d :- go(d).


/* This rule tells how to move in a given direction. */
		
go(Direction) :-
        whereyouare(Here),
        walk(Here, Direction, There),
        retract(i_am_at(Here)),
        assert(i_am_at(There)),
        view.

		
go(_) :-
        write('You can''t go that way.').


/* This rule tells how to look about you. */

view :-
        whereyouare(Setting),
        describe(Setting),
        nl,
        notice_objects_at(Setting),
        nl.

/* These rules set up a loop to mention all the objects
   in your vicinity. */


notice_objects_at(Setting) :-
        at(X, Setting),
        write('There is a '), write(X), write(' here.'), nl,
        fail.
		
		
notice_objects_at(_).


/* These rules tell how to handle killing the lion and the spider. */


/* This rule tells how to die. */


/* Under UNIX, the "halt." command quits Prolog but does not
   remove the output window. On a PC, however, the window
   disappears before the final output can be seen. Hence this
   routine requests the user to perform the final "halt." */


/* Rule explains rules of game */

detailedrules :-
        nl,
        write('Enter commands using standard Prolog syntax.'), nl,
        write('Available commands are:'), nl,
        write('start.                   -- to start the game.'), nl,
        write('n.  s.  e.  w.  u.  d.   -- to go in that direction.'), nl,
        write('take(Object).            -- to pick up an object.'), nl,
        write('drop(Object).            -- to put down an object.'), nl,
        write('kill.                    -- to attack an enemy.'), nl,
        write('view.                    -- to look around you again.'), nl,
        write('instructions.            -- to see this message again.'), nl,
        write('halt.                    -- to end the game and quit.'), nl,
        nl.
		
		

/* This rule prints out instructions and tells where you are. */

start :-
        detailedrules,
        view.


/* These rules describe the various rooms.  Depending on
   circumstances, a room may have more than one description. */

describe(meadow) :-
        at(pieceofgold, in_hand),
        write('Congratulations!!  You have recovered the pieceofgold'), nl,
        write('and won the game.'), nl,
        finish.		
		
		
describe(meadow) :-
        write('You are in a meadow.  To the north is the dark mouth'), nl,
        write('of a cave; to the south is a small building.  Your'), nl,
        write('assignment, should you decide to accept it, is to'), nl,
        write('recover the famed Bar-Abzad ruby and return it to'), nl,
        write('this meadow.'), nl.

describe(building) :-
        write('You are in a small building.  The exit is to the north.'), nl,
        write('There is a barred door to the west, but it seems to be'), nl,
        write('unlocked.  There is a smaller door to the east.'), nl.

describe(cage) :-
        write('You are in a lion''s den!  The lion has a lean and'), nl,
        write('hungry look.  You better get out of here!'), nl.

describe(closet) :-
        write('This is nothing but an old storage closet.'), nl.

describe(cave_entrance) :-
        write('You are in the mouth of a dank cave.  The exit is to'), nl,
        write('the south; there is a large, dark, round passage to'), nl,
        write('the east.'), nl.

		
describe(bridge) :-
        alive(troll),
        at(pieceofgold, in_hand),
        write('The spider sees you with the pieceofgold and attacks!!!'), nl,
        write('    ...it is over in seconds....').

describe(bridge) :-
        alive(troll),
        write('There is a giant spider here!  One hairy leg, about the'), nl,
        write('size of a telephone pole, is directly in front of you!'), nl,
        write('I would advise you to leave promptly and quietly....'), nl.


describe(bridge) :-
        write('Yecch!  There is a giant spider here, twitching.'), nl.

describe(troll) :-
        alive(troll),
        write('You are on top of a giant spider, standing in a rough'), nl,
        write('mat of coarse hair.  The smell is awful.'), nl.

describe(troll) :-
        write('Oh, gross!  You''re on top of a giant dead spider!'), nl.
