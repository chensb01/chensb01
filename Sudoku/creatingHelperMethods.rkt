#lang racket
(define Matrix (list (list 0 2 5 0 0 1 0 0 0) (list 1 0 4 2 5 0 0 0 0) (list 0 0 6 0 0 4 2 1 0) (list 0 5 0 0 0 0 3 2 0) (list 6 0 0 0 2 0 0 0 9)(list 0 8 7 0 0 0 0 6 0) (list 0 9 1 5 0 0 6 0 0)(list 0 0 0 0 7 8 1 0 3) (list 0 0 0 6 0 0 5 9 0)))

(define PossibleMatrix (list))
(define SolvedMatrix (list(list)))
(define alteringList (list))
(define buildMatrix (list(list)))
;(define Box1 (list(list)))
(define TransformedMatrix (list(list)))

(define (helper-sum-items list numSoFar); brings back number of items in a list
   (define (iter numSoFar restOfList)
      (if (null? restOfList)  
          numSoFar
        (iter (+ numSoFar 1) (cdr restOfList))
      )
   )
   (iter 0 list)
)
(helper-sum-items Matrix 0); brings back number of items in a Matrix = 9

(define (num_items X)
  (cond 
    ((null? X) 0)
    ((list? (car X)) (+ (num_items (car X)) (num_items (cdr X))))
    (else (+ 1 (num_items (cdr X))))))


(define (solve lst)
    (foldr (lambda (element next)
            (if (helper-sum-items element 0) > 0)
          (cond ((list? element)

               next)))
         empty lst))


  (define (CheckIfMember lst number)
    (num_items lst)
    (cond ((member number lst) '#t) (else '#f))
    )


(solve Matrix)
(CheckIfMember (car Matrix) 5)