#lang racket
 ; '((0 2 5 0 0 1 0 0 0)
(define Matrix                   
  '((0 2 5 0 0 1 0 0 0)
    (1 0 4 2 5 0 0 0 0)
    (0 0 6 0 0 4 2 1 0)
    (0 5 0 0 0 0 3 2 0)
    (6 0 0 0 2 0 0 0 9)
    (0 8 7 0 0 0 0 6 0)
    (0 9 1 5 0 0 6 0 0)
    (0 0 0 0 7 8 1 0 3)
    (0 0 0 6 0 0 5 9 0)))

(define c1 '())
(define c2 '())
(define c3 '())
(define c4 '())
(define c5 '())
(define c6 '())
(define c7 '())
(define c8 '())
(define c9 '())
(define c10 '())
(define c19 '())
(define c28 '())
(define c37 '())
(define c46 '())
(define c55 '())
(define c64 '())
(define c73 '())

(define mergeAllRow1 '())
(define mergeAllRow2 '())
(define mergeAllRow3 '())
(define mergeAllRow4 '())
(define mergeAllRow5 '())
(define mergeAllRow6 '())
(define mergeAllRow7 '())
(define mergeAllRow8 '())
(define mergeAllRow9 '())
(define c11 '())
(define c20 '())
(define c29 '())
(define c38 '())
(define c47 '())
(define c56 '())
(define c65 '())
(define c74 '())


(define c12 '())
(define c21 '())
(define c30 '())
(define c39 '())
(define c48 '())
(define c57 '())
(define c66 '())
(define c75 '())


(define c13 '())
(define c22 '())
(define c31 '())
(define c40 '())
(define c49 '())
(define c58 '())
(define c67 '())
(define c76 '())


(define c14 '())
(define c23 '())
(define c32 '())
(define c41 '())
(define c50 '())
(define c59 '())
(define c68 '())
(define c77 '())


(define c15 '())
(define c24 '())
(define c33 '())
(define c42 '())
(define c51 '())
(define c60 '())
(define c69 '())
(define c78 '())


(define c16 '())
(define c25 '())
(define c34 '())
(define c43 '())
(define c52 '())
(define c61 '())
(define c70 '())
(define c79 '())


(define c17 '())
(define c26 '())
(define c35 '())
(define c44 '())
(define c53 '())
(define c62 '())
(define c71 '())
(define c80 '())


(define c18 '())
(define c27 '())
(define c36 '())
(define c45 '())
(define c54 '())
(define c63 '())
(define c72 '())
(define c81 '())
(define PossibleMatrix '())
(define SolvedMatrix '())
(define row1 '())
(define newrow1Element '())
(define row1_rec1 '())
(define row1_rec2 '())
(define row1_rec3 '())
(define row1_rec4 '())
(define row1_rec5 '())
(define row1_rec6 '())
(define row1_rec7 '())
(define row1_rec8 '())
(define row1_rec9 '())

(define row2_rec1 '())
(define row2_rec2 '())
(define row2_rec3 '())
(define row2_rec4 '())
(define row2_rec5 '())
(define row2_rec6 '())
(define row2_rec7 '())
(define row2_rec8 '())
(define row2_rec9 '())

(define row3_rec1 '())
(define row3_rec2 '())
(define row3_rec3 '())
(define row3_rec4 '())
(define row3_rec5 '())
(define row3_rec6 '())
(define row3_rec7 '())
(define row3_rec8 '())
(define row3_rec9 '())

(define row4_rec1 '())
(define row4_rec2 '())
(define row4_rec3 '())
(define row4_rec4 '())
(define row4_rec5 '())
(define row4_rec6 '())
(define row4_rec7 '())
(define row4_rec8 '())
(define row4_rec9 '())

(define row5_rec1 '())
(define row5_rec2 '())
(define row5_rec3 '())
(define row5_rec4 '())
(define row5_rec5 '())
(define row5_rec6 '())
(define row5_rec7 '())
(define row5_rec8 '())
(define row5_rec9 '())

(define row6_rec1 '())
(define row6_rec2 '())
(define row6_rec3 '())
(define row6_rec4 '())
(define row6_rec5 '())
(define row6_rec6 '())
(define row6_rec7 '())
(define row6_rec8 '())
(define row6_rec9 '())

(define row7_rec1 '())
(define row7_rec2 '())
(define row7_rec3 '())
(define row7_rec4 '())
(define row7_rec5 '())
(define row7_rec6 '())
(define row7_rec7 '())
(define row7_rec8 '())
(define row7_rec9 '())

(define row8_rec1 '())
(define row8_rec2 '())
(define row8_rec3 '())
(define row8_rec4 '())
(define row8_rec5 '())
(define row8_rec6 '())
(define row8_rec7 '())
(define row8_rec8 '())
(define row8_rec9 '())

(define row9_rec1 '())
(define row9_rec2 '())
(define row9_rec3 '())
(define row9_rec4 '())
(define row9_rec5 '())
(define row9_rec6 '())
(define row9_rec7 '())
(define row9_rec8 '())
(define row9_rec9 '())


(define row2 '())
(define row3 '())
(define row4 '())
(define row5 '())
(define row6 '())
(define row7 '())
(define row8 '())
(define row9 '())
(define newelement '())

(define column1 '())
(define newcolumn '())
(define column2 '())
(define column3 '())
(define column4 '())
(define column5 '())
(define column6 '())
(define column7 '())
(define column8 '())
(define column9 '())

(define column1_rec1 '())
(define column1_rec2 '())
(define column1_rec3 '())
(define column1_rec4 '())
(define column1_rec5 '())
(define column1_rec6 '())
(define column1_rec7 '())
(define column1_rec8 '())
(define column1_rec9 '())

(define column2_rec1 '())
(define column2_rec2 '())
(define column2_rec3 '())
(define column2_rec4 '())
(define column2_rec5 '())
(define column2_rec6 '())
(define column2_rec7 '())
(define column2_rec8 '())
(define column2_rec9 '())

(define column3_rec1 '())
(define column3_rec2 '())
(define column3_rec3 '())
(define column3_rec4 '())
(define column3_rec5 '())
(define column3_rec6 '())
(define column3_rec7 '())
(define column3_rec8 '())
(define column3_rec9 '())

(define column4_rec1 '())
(define column4_rec2 '())
(define column4_rec3 '())
(define column4_rec4 '())
(define column4_rec5 '())
(define column4_rec6 '())
(define column4_rec7 '())
(define column4_rec8 '())
(define column4_rec9 '())

(define column5_rec1 '())
(define column5_rec2 '())
(define column5_rec3 '())
(define column5_rec4 '())
(define column5_rec5 '())
(define column5_rec6 '())
(define column5_rec7 '())
(define column5_rec8 '())
(define column5_rec9 '())

(define column6_rec1 '())
(define column6_rec2 '())
(define column6_rec3 '())
(define column6_rec4 '())
(define column6_rec5 '())
(define column6_rec6 '())
(define column6_rec7 '())
(define column6_rec8 '())
(define column6_rec9 '())

(define column7_rec1 '())
(define column7_rec2 '())
(define column7_rec3 '())
(define column7_rec4 '())
(define column7_rec5 '())
(define column7_rec6 '())
(define column7_rec7 '())
(define column7_rec8 '())
(define column7_rec9 '())

(define column8_rec1 '())
(define column8_rec2 '())
(define column8_rec3 '())
(define column8_rec4 '())
(define column8_rec5 '())
(define column8_rec6 '())
(define column8_rec7 '())
(define column8_rec8 '())
(define column8_rec9 '())

(define column9_rec1 '())
(define column9_rec2 '())
(define column9_rec3 '())
(define column9_rec4 '())
(define column9_rec5 '())
(define column9_rec6 '())
(define column9_rec7 '())
(define column9_rec8 '())
(define column9_rec9 '())


(define box1 '())
(define box2 '())
(define box3 '())
(define box4 '())
(define box5 '())
(define box6 '())
(define box7 '())
(define box8 '())
(define box9 '())

(define box1_rec1 '())
(define box2_rec1 '())
(define box3_rec1 '())
(define box4_rec1 '())
(define box5_rec1 '())
(define box6_rec1 '())
(define box7_rec1 '())
(define box8_rec1 '())
(define box9_rec1 '())


(define buildMatrix (list'()))
(define TransformedMatrix (list'()))


(define (addToList lst)
    (foldl (lambda (element next)
          (cond ((list? element)
               (if (equal? (range 1 10) element)
                   (set! buildMatrix (append buildMatrix (list (list (list  (range 1 10))))))
                    (cons element next))
                (if (not(equal? (range 1 10) element))
                   (set! buildMatrix (append buildMatrix (list (list element))))
                    (cons element next))
               next)))
         empty lst))


(define (createRowColsAndBoxes lst numSoFar)
      (foldl (lambda (element next)
               (cond ((list? element)
                      (set! numSoFar (+ 1 numSoFar))
                       (+ numSoFar 1)
                        (if(member numSoFar '(1 2 3 4 5 6 7 8 9))
                          (set! row1 (append row1 (list element)))
                          (cons element next))
                        (if(member numSoFar '(10 11 12 13 14 15 16 17 18))
                           (set! row2 (append row2 (list element)))
                           (cons element next))
                        (if(member numSoFar '(19 20 21 22 23 24 25 26 27))
                           (set! row3 (append row3 (list element)))
                           (cons element next))
                        (if(member numSoFar '(28 29 30 31 32 33 34 35 36))
                           (set! row4 (append row4 (list element)))
                           (cons element next))
                        (if(member numSoFar '(37 38 39 40 41 42 43 44 45))
                           (set! row5 (append row5 (list element)))
                           (cons element next))
                        (if(member numSoFar '(46 47 48 49 50 51 52 53 54))
                           (set! row6 (append row6 (list element)))
                           (cons element next))
                        (if(member numSoFar '(55 56 57 58 59 60 61 62 63))
                           (set! row7 (append row7 (list element)))
                           (cons element next))
                        (if(member numSoFar '(64 65 66 67 68 69 70 71 72))
                           (set! row8 (append row8 (list element)))
                           (cons element next))
                        (if(member numSoFar '(73 74 75 76 77 78 79 80 81))
                           (set! row9 (append row9 (list element)))
                           (cons element next))
                        (if(member numSoFar '(1 10 19 28 37 46 55 64 73))
                           (set! column1 (append column1 (list element)))
                           (cons element next))
                        (if(member numSoFar '(2 11 20 29 38 47 56 65 74))
                           (set! column2 (append column2 (list element)))
                           (cons element next))
                        (if(member numSoFar '(3 12 21 30 39 48 57 66 75))
                           (set! column3 (append column3 (list element)))
                           (cons element next))
                        (if(member numSoFar '(4 13 22 31 40 49 58 67 76))
                           (set! column4 (append column4 (list element)))
                           (cons element next))
                        (if(member numSoFar '(5 14 23 32 41 50 59 68 77))
                           (set! column5 (append column5 (list element)))
                           (cons element next))
                        (if(member numSoFar '(6 15 24 33 42 51 60 69 78))
                           (set! column6 (append column6 (list element)))
                           (cons element next))
                        (if(member numSoFar '(7 16 25 34 43 52 61 70 79))
                           (set! column7 (append column7 (list element)))
                           (cons element next))
                        (if(member numSoFar '(8 17 26 35 44 53 62 71 80))
                           (set! column8 (append column8 (list element)))
                           (cons element next))
                        (if(member numSoFar '(9 18 27 36 45 54 63 72 81))
                           (set! column9 (append column9 (list element)))
                           (cons element next))
                        ;boxes
                        (if(member numSoFar '(1 2 3 10 11 12 19 20 21))
                           (set! box1 (append box1 (list element)))
                           (cons element next))
                        (if(member numSoFar '(4 5 6 13 14 15 22 23 24))
                           (set! box2 (append box2 (list element)))
                           (cons element next))
                        (if(member numSoFar '(7 8 9 16 17 18 25 26 27))
                           (set! box3 (append box3 (list element)))
                           (cons element next))
                        (if(member numSoFar '(28 29 30 37 38 39 46 47 48))
                           (set! box4 (append box4 (list element)))
                           (cons element next))
                        (if(member numSoFar '(31 32 33 40 41 42 49 50 51))
                           (set! box5 (append box5 (list element)))
                           (cons element next))
                        (if(member numSoFar '(34 35 36 43 44 45 52 53 54))
                           (set! box6 (append box6 (list element)))
                           (cons element next))
                        (if(member numSoFar '(55 56 57 64 65 66 73 74 75))
                           (set! box7 (append box7 (list element)))
                           (cons element next))
                        (if(member numSoFar '(58 59 60 67 68 69 76 77 78))
                           (set! box8 (append box8 (list element)))
                           (cons element next))
                        (if(member numSoFar '(61 62 63 70 71 72 79 80 81))
                           (set! box9 (append box9 (list element)))
                           (cons element next))
               next)))
         empty lst))



(define remove-nondup
  (λ (ls)
    (reverse
      (let loop ([ls ls] [found '()] [acc '()])
        (cond
          [(null? ls)
            acc]
          [(memq (car ls) found)
            (loop (cdr ls)
                  found
                  (if (memq (car ls) acc)
                      acc
                      (cons (car ls) acc)))]
          [else
            (loop (cdr ls)
                  (cons (car ls) found)
                  acc)])))))







(define (mergeNonSingleLists lst numSoFar)
  (define numSoFar 0)
      (foldl (lambda (element next)
               (cond ((list? element)
                      (set! numSoFar (+ 1 numSoFar))
                       (+ numSoFar 1)
                (if(not(equal? 1 (length element))); now below checkinRows passing one list in the list of list only if the element has a length of 1
                       (mergeElements element numSoFar); element is a number; calling checkinrows method passing element which is only 1 element and the position of the element
                        (cons element next))
                
               next)))
         empty lst)

  )



(define (mergeElements listnumber numSoFar)
      (foldl (lambda (element next)
               (cond ((number? numSoFar)
                      ;;checking if the reiteration; position in the matrix is a member of the statement list, and also checking that the element at that position is equal to 1 being the
                      ;;ridid element
                      (if (and (member numSoFar (list 1 2 3 4 5 6 7 8 9)) (not(equal? 1 (length listnumber))))
                          (begin
                            (set! mergeAllRow1 (append mergeAllRow1 listnumber)))
                          (cons element next))
                      (if (and (member numSoFar (list 10 11 12 13 14 15 16 17 18)) (not(equal? 1 (length listnumber))))
                          (begin
                            (set! mergeAllRow2 (append mergeAllRow2 listnumber)))
                          (cons element next))
                      (if (and (member numSoFar (list 19 20 21 22 23 24 25 26 27)) (not(equal? 1 (length listnumber))))
                          (begin
                            (set! mergeAllRow3 (append mergeAllRow3 listnumber)))
                          (cons element next))                    
                      (if (and (member numSoFar (list 28 29 30 31 32 33 34 35 36)) (not(equal? 1 (length listnumber))))
                          (begin
                            (set! mergeAllRow4 (append mergeAllRow4 listnumber)))
                          (cons element next))
                       (if (and (member numSoFar (list 37 38 39 40 41 42 43 44 45)) (not(equal? 1 (length listnumber))))
                          (begin
                            (set! mergeAllRow5 (append mergeAllRow5 listnumber)))
                          (cons element next))                    
                       (if (and (member numSoFar (list 46 47 48 49 50 51 52 53 54)) (not(equal? 1 (length listnumber))))
                          (begin
                            (set! mergeAllRow6 (append mergeAllRow6 listnumber)))
                          (cons element next))                    
                       (if (and (member numSoFar (list 55 56 57 58 59 60 61 62 63)) (not(equal? 1 (length listnumber))))
                          (begin
                            (set! mergeAllRow7 (append mergeAllRow7 listnumber)))
                          (cons element next))                    
                       (if (and (member numSoFar (list 64 65 66 67 68 69 70 71 72)) (not(equal? 1 (length listnumber))))
                          (begin
                            (set! mergeAllRow8 (append mergeAllRow8 listnumber)))
                          (cons element next))                                       
                       (if (and (member numSoFar (list 73 74 75 76 77 78 79 80 81)) (not(equal? 1 (length listnumber))))
                          (begin
                            (set! mergeAllRow9 (append mergeAllRow9 listnumber)))
                          (cons element next))
                      ;;checking if the iteration; position in the matrix is a member of the statement list, and also checking that the element at that position is
                       ;not equal to 1 being the element which needs to have the listnumber removed from the elements
                      
                  
                          (cons element next))))
         empty listnumber))






(define (StickBack)
  (set! PossibleMatrix '())
   (set! PossibleMatrix (append PossibleMatrix row1_rec1))
   (set! PossibleMatrix (append PossibleMatrix row1_rec2))
   (set! PossibleMatrix (append PossibleMatrix row1_rec3))
   (set! PossibleMatrix (append PossibleMatrix row1_rec4))
   (set! PossibleMatrix (append PossibleMatrix row1_rec5))
   (set! PossibleMatrix (append PossibleMatrix row1_rec6))   
   (set! PossibleMatrix (append PossibleMatrix row1_rec7))
   (set! PossibleMatrix (append PossibleMatrix row1_rec8))
   (set! PossibleMatrix (append PossibleMatrix row1_rec9))
   (set! PossibleMatrix (append PossibleMatrix row2_rec1))
   (set! PossibleMatrix (append PossibleMatrix row2_rec2))
   (set! PossibleMatrix (append PossibleMatrix row2_rec3))
   (set! PossibleMatrix (append PossibleMatrix row2_rec4))
   (set! PossibleMatrix (append PossibleMatrix row2_rec5))
   (set! PossibleMatrix (append PossibleMatrix row2_rec6))   
   (set! PossibleMatrix (append PossibleMatrix row2_rec7))
   (set! PossibleMatrix (append PossibleMatrix row2_rec8))
   (set! PossibleMatrix (append PossibleMatrix row2_rec9))
   (set! PossibleMatrix (append PossibleMatrix row3_rec1))
   (set! PossibleMatrix (append PossibleMatrix row3_rec2))
   (set! PossibleMatrix (append PossibleMatrix row3_rec3))
   (set! PossibleMatrix (append PossibleMatrix row3_rec4))
   (set! PossibleMatrix (append PossibleMatrix row3_rec5))
   (set! PossibleMatrix (append PossibleMatrix row3_rec6))   
   (set! PossibleMatrix (append PossibleMatrix row3_rec7))
   (set! PossibleMatrix (append PossibleMatrix row3_rec8))
   (set! PossibleMatrix (append PossibleMatrix row3_rec9))
   (set! PossibleMatrix (append PossibleMatrix row4_rec1))
   (set! PossibleMatrix (append PossibleMatrix row4_rec2))
   (set! PossibleMatrix (append PossibleMatrix row4_rec3))
   (set! PossibleMatrix (append PossibleMatrix row4_rec4))
   (set! PossibleMatrix (append PossibleMatrix row4_rec5))
   (set! PossibleMatrix (append PossibleMatrix row4_rec6))   
   (set! PossibleMatrix (append PossibleMatrix row4_rec7))
   (set! PossibleMatrix (append PossibleMatrix row4_rec8))
   (set! PossibleMatrix (append PossibleMatrix row4_rec9))
   (set! PossibleMatrix (append PossibleMatrix row5_rec1))
   (set! PossibleMatrix (append PossibleMatrix row5_rec2))
   (set! PossibleMatrix (append PossibleMatrix row5_rec3))
   (set! PossibleMatrix (append PossibleMatrix row5_rec4))
   (set! PossibleMatrix (append PossibleMatrix row5_rec5))
   (set! PossibleMatrix (append PossibleMatrix row5_rec6))   
   (set! PossibleMatrix (append PossibleMatrix row5_rec7))
   (set! PossibleMatrix (append PossibleMatrix row5_rec8))
   (set! PossibleMatrix (append PossibleMatrix row5_rec9))
   (set! PossibleMatrix (append PossibleMatrix row6_rec1))
   (set! PossibleMatrix (append PossibleMatrix row6_rec2))
   (set! PossibleMatrix (append PossibleMatrix row6_rec3))
   (set! PossibleMatrix (append PossibleMatrix row6_rec4))
   (set! PossibleMatrix (append PossibleMatrix row6_rec5))
   (set! PossibleMatrix (append PossibleMatrix row6_rec6))   
   (set! PossibleMatrix (append PossibleMatrix row6_rec7))
   (set! PossibleMatrix (append PossibleMatrix row6_rec8))
   (set! PossibleMatrix (append PossibleMatrix row6_rec9))
   (set! PossibleMatrix (append PossibleMatrix row7_rec1))
   (set! PossibleMatrix (append PossibleMatrix row7_rec2))
   (set! PossibleMatrix (append PossibleMatrix row7_rec3))
   (set! PossibleMatrix (append PossibleMatrix row7_rec4))
   (set! PossibleMatrix (append PossibleMatrix row7_rec5))
   (set! PossibleMatrix (append PossibleMatrix row7_rec6))   
   (set! PossibleMatrix (append PossibleMatrix row7_rec7))
   (set! PossibleMatrix (append PossibleMatrix row7_rec8))
   (set! PossibleMatrix (append PossibleMatrix row7_rec9))
   (set! PossibleMatrix (append PossibleMatrix row8_rec1))
   (set! PossibleMatrix (append PossibleMatrix row8_rec2))
   (set! PossibleMatrix (append PossibleMatrix row8_rec3))
   (set! PossibleMatrix (append PossibleMatrix row8_rec4))
   (set! PossibleMatrix (append PossibleMatrix row8_rec5))
   (set! PossibleMatrix (append PossibleMatrix row8_rec6))   
   (set! PossibleMatrix (append PossibleMatrix row8_rec7))
   (set! PossibleMatrix (append PossibleMatrix row8_rec8))
   (set! PossibleMatrix (append PossibleMatrix row8_rec9))
   (set! PossibleMatrix (append PossibleMatrix row9_rec1))
   (set! PossibleMatrix (append PossibleMatrix row9_rec2))
   (set! PossibleMatrix (append PossibleMatrix row9_rec3))
   (set! PossibleMatrix (append PossibleMatrix row9_rec4))
   (set! PossibleMatrix (append PossibleMatrix row9_rec5))
   (set! PossibleMatrix (append PossibleMatrix row9_rec6))   
   (set! PossibleMatrix (append PossibleMatrix row9_rec7))
   (set! PossibleMatrix (append PossibleMatrix row9_rec8))
   (set! PossibleMatrix (append PossibleMatrix row9_rec9))
   )


 (define (NoNeedToRemove lst); 
  (member lst (list 1 2 3 4)) )


(define (removeElement x ls)
  (if (null? ls)
      '()
      (let ((h (car ls)))
        ((if (eqv? x h)
            (lambda (y) y)
            (lambda (y) (cons h y)))
         (remove x (cdr ls))))))

; now below checkinRows passing one element from the list of lists from the PossibleMatrix but passing only if the element has a length of 1; so if the element is already filled in such as 2 etc.

(define (solve lst)
  (define numSoFar 0)
  (createRowColsAndBoxes lst numSoFar)
      (foldl (lambda (element next)
               (cond ((list? element)
                      (set! numSoFar (+ 1 numSoFar))
                       (+ numSoFar 1)
                (if(equal? 1 (length element)); now below checkinRows passing one list in the list of list only if the element has a length of 1
                       (checkAll element numSoFar); element is a number; calling checkinrows method passing element which is only 1 element and the position of the element
                        (cons element next))
                
               next)))
         empty lst)
    (StickBack)
  (mergeNonSingleLists PossibleMatrix 0)
  (StickBack)

 ; PossibleMatrix
 ;(solve PossibleMatrix)
  )

(define (checkAll listnumber numSoFar)
      (foldl (lambda (element next)
               (cond ((number? numSoFar)
                      ;;checking if the reiteration; position in the matrix is a member of the statement list, and also checking that the element at that position is equal to 1 being the
                      ;;ridid element
                      (if (and (member numSoFar (list 1 2 3 4 5 6 7 8 9)) (equal? 1 (length listnumber)))
                          (begin
                          (AppendFirmElementToRow1 numSoFar listnumber row1))
                          (cons element next))
                      (if (and (member numSoFar (list 10 11 12 13 14 15 16 17 18)) (equal? 1 (length listnumber)))
                          (begin
                          (AppendFirmElementToRow2 numSoFar listnumber row2))
                          (cons element next))
                      (if (and (member numSoFar (list 19 20 21 22 23 24 25 26 27)) (equal? 1 (length listnumber)))
                          (begin
                          (AppendFirmElementToRow3 numSoFar listnumber row3))
                          (cons element next))                      
                      (if (and (member numSoFar (list 28 29 30 31 32 33 34 35 36)) (equal? 1 (length listnumber)))
                          (begin
                          (AppendFirmElementToRow4 numSoFar listnumber row4))
                          (cons element next))
                       (if (and (member numSoFar (list 37 38 39 40 41 42 43 44 45)) (equal? 1 (length listnumber)))
                          (begin
                          (AppendFirmElementToRow5 numSoFar listnumber row5))
                          (cons element next))                     
                       (if (and (member numSoFar (list 46 47 48 49 50 51 52 53 54)) (equal? 1 (length listnumber)))
                          (begin
                          (AppendFirmElementToRow6 numSoFar listnumber row6))
                          (cons element next))                      
                       (if (and (member numSoFar (list 55 56 57 58 59 60 61 62 63)) (equal? 1 (length listnumber)))
                          (begin
                          (AppendFirmElementToRow7 numSoFar listnumber row7))
                          (cons element next))                     
                       (if (and (member numSoFar (list 64 65 66 67 68 69 70 71 72)) (equal? 1 (length listnumber)))
                          (begin
                          (AppendFirmElementToRow8 numSoFar listnumber row8))
                          (cons element next))                                         
                       (if (and (member numSoFar (list 73 74 75 76 77 78 79 80 81)) (equal? 1 (length listnumber)))
                          (begin
                          (AppendFirmElementToRow9 numSoFar listnumber row9))
                          (cons element next))
                      ;;checking if the iteration; position in the matrix is a member of the statement list, and also checking that the element at that position is
                       ;not equal to 1 being the element which needs to have the listnumber removed from the elements
                      
                      (if (and (member numSoFar (list 1 2 3 4 5 6 7 8 9)) (not(equal? 1 (length listnumber))))
                          (begin
                          (RemoveAndAppendFirmElementToRow1 numSoFar listnumber listnumber row1))
                          (cons element next))
                      (if (and (member numSoFar (list 10 11 12 13 14 15 16 17 18)) (not(equal? 1 (length listnumber))))
                          (begin
                          (RemoveAndAppendFirmElementToRow2 numSoFar listnumber row2))
                          (cons element next))
                      (if (and (member numSoFar (list 19 20 21 22 23 24 25 26 27)) (not(equal? 1 (length listnumber))))
                          (begin
                          (RemoveAndAppendFirmElementToRow3 numSoFar listnumber row3))
                          (cons element next))                      
                      (if (and (member numSoFar (list 28 29 30 31 32 33 34 35 36)) (not(equal? 1 (length listnumber))))
                          (begin
                          (RemoveAndAppendFirmElementToRow4 numSoFar listnumber row4))
                          (cons element next))
                       (if (and (member numSoFar (list 37 38 39 40 41 42 43 44 45)) (not(equal? 1 (length listnumber))))
                          (begin
                          (RemoveAndAppendFirmElementToRow5 numSoFar listnumber row5))
                          (cons element next))                     
                       (if (and (member numSoFar (list 46 47 48 49 50 51 52 53 54)) (not(equal? 1 (length listnumber))))
                          (begin
                          (RemoveAndAppendFirmElementToRow6 numSoFar listnumber row6))
                          (cons element next))                      
                       (if (and (member numSoFar (list 55 56 57 58 59 60 61 62 63)) (not(equal? 1 (length listnumber))))
                          (begin
                          (RemoveAndAppendFirmElementToRow7 numSoFar listnumber row7))
                          (cons element next))                     
                       (if (and (member numSoFar (list 64 65 66 67 68 69 70 71 72)) (not(equal? 1 (length listnumber))))
                          (begin
                          (RemoveAndAppendFirmElementToRow8 numSoFar listnumber row8))
                          (cons element next))                                         
                       (if (and (member numSoFar (list 73 74 75 76 77 78 79 80 81)) (not(equal? 1 (length listnumber))))
                          (begin
                          (RemoveAndAppendFirmElementToRow9 numSoFar element row9))
                          (cons element next))   )))
         empty listnumber))



(define (AppendFirmElementToRow1 numSoFar number lst)
    (foldl (lambda (element next)
          (cond ((list? element)
              (if(equal? numSoFar 1)
                          (begin
                          (set! row1_rec1 (append row1_rec1 (list number))))
                          (cons element next))
              (if(equal? numSoFar 2)
                          (begin
                          (set! row1_rec2 (append row1_rec2 (list number))))
                          (cons element next))
              (if(equal? numSoFar 3)
                          (begin
                          (set! row1_rec3 (append row1_rec3 (list number))))
                          (cons element next))
               (if(equal? numSoFar 4)
                          (begin
                          (set! row1_rec4 (append row1_rec4 (list number))))
                          (cons element next))
               (if(equal? numSoFar 5)
                          (begin
                          (set! row1_rec5 (append row1_rec5 (list number))))
                          (cons element next))
                (if(equal? numSoFar 6)
                          (begin
                          (set! row1_rec6 (append row1_rec6 (list element))))
                          (cons element next))             
               (if(equal? numSoFar 7)
                          (begin
                          (set! row1_rec7 (append row1_rec7 (list number))))
                          (cons element next))
               (if(equal? numSoFar 8)
                          (begin
                          (set! row1_rec8 (append row1_rec8 (list number))))
                          (cons element next))
                  (if(equal? numSoFar 9)
                          (begin
                          (set! row1_rec9 (append row1_rec9 (list number))))
                          (cons element next))            
               next)))
         empty lst))


(define (AppendFirmElementToRow2 numSoFar number lst)
    (foldl (lambda (element next)
          (cond ((list? element)
              (if(equal? numSoFar 10)
                          (begin
                          (set! row2_rec1 (append row2_rec1 (list number))))
                          (cons element next))
              (if(equal? numSoFar 11)
                          (begin
                          (set! row2_rec2 (append row2_rec2 (list number))))
                          (cons element next))
              (if(equal? numSoFar 12)
                          (begin
                          (set! row2_rec3 (append row2_rec3 (list number))))
                          (cons element next))
               (if(equal? numSoFar 13)
                          (begin
                          (set! row2_rec4 (append row2_rec4 (list number))))
                          (cons element next))
               (if(equal? numSoFar 14)
                          (begin
                          (set! row2_rec5 (append row2_rec5 (list number))))
                          (cons element next))
                (if(equal? numSoFar 15)
                          (begin
                          (set! row2_rec6 (append row2_rec6 (list number))))
                          (cons element next))             
               (if(equal? numSoFar 16)
                          (begin
                          (set! row2_rec7 (append row2_rec7 (list number))))
                          (cons element next))
               (if(equal? numSoFar 17)
                          (begin
                          (set! row2_rec8 (append row2_rec8 (list number))))
                          (cons element next))
                  (if(equal? numSoFar 18)
                          (begin
                          (set! row2_rec9 (append row2_rec9 (list number))))
                          (cons element next))            
               next)))
         empty lst))

(define (AppendFirmElementToRow3 numSoFar number lst)
    (foldl (lambda (element next)
          (cond ((list? element)
              (if(equal? numSoFar 19)
                          (begin
                          (set! row3_rec1 (append row3_rec1 (list number))))
                          (cons element next))
              (if(equal? numSoFar 20)
                          (begin
                          (set! row3_rec2 (append row3_rec2 (list number))))
                          (cons element next))
              (if(equal? numSoFar 21)
                          (begin
                          (set! row3_rec3 (append row3_rec3 (list number))))
                          (cons element next))
               (if(equal? numSoFar 22)
                          (begin
                          (set! row3_rec4 (append row3_rec4 (list number))))
                          (cons element next))
               (if(equal? numSoFar 23)
                          (begin
                          (set! row3_rec5 (append row3_rec5 (list number))))
                          (cons element next))
               (if(equal? numSoFar 24)
                          (begin
                          (set! row3_rec6 (append row3_rec6 (list number))))
                          (cons element next))
               (if(equal? numSoFar 25)
                          (begin
                          (set! row3_rec7 (append row3_rec7 (list number))))
                          (cons element next))
                (if(equal? numSoFar 25)
                          (begin
                          (set! row3_rec8 (append row3_rec8 (list number))))
                          (cons element next))             
               (if(equal? numSoFar 26)
                          (begin
                          (set! row3_rec8 (append row3_rec8 (list number))))
                          (cons element next))
               (if(equal? numSoFar 27)
                          (begin
                          (set! row3_rec9 (append row3_rec9 (list number))))
                          (cons element next))
                             
               next)))
         empty lst))


(define (AppendFirmElementToRow4 numSoFar number lst)
    (foldl (lambda (element next)
          (cond ((list? element)
              (if(equal? numSoFar 28)
                          (begin
                          (set! row4_rec1 (append row4_rec1 (list number))))
                          (cons element next))
              (if(equal? numSoFar 29)
                          (begin
                          (set! row4_rec2 (append row4_rec2 (list number))))
                          (cons element next))
              (if(equal? numSoFar 30)
                          (begin
                          (set! row4_rec3 (append row4_rec3 (list number))))
                          (cons element next))
               (if(equal? numSoFar 31)
                          (begin
                          (set! row4_rec4 (append row4_rec4 (list number))))
                          (cons element next))
               (if(equal? numSoFar 32)
                          (begin
                          (set! row4_rec5 (append row4_rec5 (list number))))
                          (cons element next))
               (if(equal? numSoFar 33)
                          (begin
                          (set! row4_rec6 (append row4_rec6 (list number))))
                          (cons element next))
               (if(equal? numSoFar 34)
                          (begin
                          (set! row4_rec7 (append row4_rec7 (list number))))
                          (cons element next))
               (if(equal? numSoFar 35)
                          (begin
                          (set! row4_rec8 (append row4_rec8 (list number))))
                          (cons element next))
               (if(equal? numSoFar 36)
                          (begin
                          (set! row4_rec9 (append row4_rec9 (list number))))
                          (cons element next))
               next)))
         empty lst))




(define (AppendFirmElementToRow5 numSoFar number lst)
    (foldl (lambda (element next)
          (cond ((list? element)
              (if(equal? numSoFar 37)
                          (begin
                          (set! row5_rec1 (append row5_rec1 (list number))))
                          (cons element next))
              (if(equal? numSoFar 38)
                          (begin
                          (set! row5_rec2 (append row5_rec2 (list number))))
                          (cons element next))
              (if(equal? numSoFar 39)
                          (begin
                          (set! row5_rec3 (append row5_rec3 (list number))))
                          (cons element next))
               (if(equal? numSoFar 40)
                          (begin
                          (set! row5_rec4 (append row5_rec4 (list number))))
                          (cons element next))
               (if(equal? numSoFar 41)
                          (begin
                          (set! row5_rec5 (append row5_rec5 (list number))))
                          (cons element next))
               (if(equal? numSoFar 42)
                          (begin
                          (set! row5_rec6 (append row5_rec6 (list number))))
                          (cons element next))
               (if(equal? numSoFar 43)
                          (begin
                          (set! row5_rec7 (append row5_rec7 (list number))))
                          (cons element next))
               (if(equal? numSoFar 44)
                          (begin
                          (set! row5_rec8 (append row5_rec8 (list number))))
                          (cons element next))
               (if(equal? numSoFar 45)
                          (begin
                          (set! row5_rec9 (append row5_rec9 (list number))))
                          (cons element next))               
               next)))
         empty lst))

(define (AppendFirmElementToRow6 numSoFar number lst)
    (foldl (lambda (element next)
          (cond ((list? element)
              (if(equal? numSoFar 46)
                          (begin
                          (set! row6_rec1 (append row6_rec1 (list number))))
                          (cons element next))
              (if(equal? numSoFar 47)
                          (begin
                          (set! row6_rec2 (append row6_rec2 (list number))))
                          (cons element next))
              (if(equal? numSoFar 48)
                          (begin
                          (set! row6_rec3 (append row6_rec3 (list number))))
                          (cons element next))
               (if(equal? numSoFar 49)
                          (begin
                          (set! row6_rec4 (append row6_rec4 (list number))))
                          (cons element next))
               (if(equal? numSoFar 50)
                          (begin
                          (set! row6_rec5 (append row6_rec5 (list number))))
                          (cons element next))
                (if(equal? numSoFar 51)
                          (begin
                          (set! row6_rec6 (append row6_rec6 (list number))))
                           (cons element next)) 
                (if(equal? numSoFar 52)
                          (begin
                          (set! row6_rec7 (append row6_rec7 (list number))))
                          (cons element next))                                     
                (if(equal? numSoFar 53)
                          (begin
                          (set! row6_rec8 (append row6_rec8 (list number))))
                          (cons element next))
                (if(equal? numSoFar 54)
                          (begin
                          (set! row6_rec9 (append row6_rec9 (list number))))
                         (cons element next)) 
               next)))
         empty lst))


(define (AppendFirmElementToRow7 numSoFar number lst)
    (foldl (lambda (element next)
          (cond ((list? element)
              (if(equal? numSoFar 55)
                          (begin
                          (set! row7_rec1 (append row7_rec1 (list number))))
                          (cons element next))
              (if(equal? numSoFar 56)
                          (begin
                          (set! row7_rec2 (append row7_rec2 (list number))))
                          (cons element next))
              (if(equal? numSoFar 57)
                          (begin
                          (set! row7_rec3 (append row7_rec3 (list number))))
                          (cons element next))
               (if(equal? numSoFar 58)
                          (begin
                          (set! row7_rec4 (append row7_rec4 (list number))))
                          (cons element next))
               (if(equal? numSoFar 59)
                          (begin
                          (set! row7_rec5 (append row7_rec5 (list number))))
                          (cons element next))
                (if(equal? numSoFar 60)
                          (begin
                          (set! row7_rec6 (append row7_rec6 (list number))))
                           (cons element next)) 
                (if(equal? numSoFar 61)
                          (begin
                          (set! row7_rec7 (append row7_rec7 (list number))))
                          (cons element next))                                     
                (if(equal? numSoFar 62)
                          (begin
                          (set! row7_rec8 (append row7_rec8 (list number))))
                          (cons element next))
                (if(equal? numSoFar 63)
                          (begin
                          (set! row7_rec9 (append row7_rec9 (list number))))
                          (cons element next)) 
               next)))
         empty lst))




(define (AppendFirmElementToRow8 numSoFar number lst)
    (foldl (lambda (element next)
          (cond ((list? element)
              (if(equal? numSoFar 64)
                          (begin
                          (set! row8_rec1 (append row8_rec1 (list number))))
                          (cons element next))
              (if(equal? numSoFar 65)
                          (begin
                          (set! row8_rec2 (append row8_rec2 (list number))))
                          (cons element next))
              (if(equal? numSoFar 66)
                          (begin
                          (set! row8_rec3 (append row8_rec3 (list number))))
                          (cons element next))
               (if(equal? numSoFar 67)
                          (begin
                          (set! row8_rec4 (append row8_rec4 (list number))))
                          (cons element next))
               (if(equal? numSoFar 68)
                          (begin
                          (set! row8_rec5 (append row8_rec5 (list number))))
                          (cons element next))
                (if(equal? numSoFar 69)
                          (begin
                          (set! row8_rec6 (append row8_rec6 (list number))))
                           (cons element next)) 
                (if(equal? numSoFar 70)
                          (begin
                          (set! row8_rec7 (append row8_rec7 (list number))))
                          (cons element next))                                     
                (if(equal? numSoFar 71)
                          (begin
                          (set! row8_rec8 (append row8_rec8 (list number))))
                          (cons element next))
                (if(equal? numSoFar 72)
                          (begin
                          (set! row8_rec9 (append row8_rec9 (list number))))
                          (cons element next)) 
               next)))
         empty lst))



(define (AppendFirmElementToRow9 numSoFar number lst)
    (foldl (lambda (element next)
          (cond ((list? element)
              (if(equal? numSoFar 73)
                          (begin
                          (set! row9_rec1 (append row9_rec1 (list number))))
                          (cons element next))
              (if(equal? numSoFar 74)
                          (begin
                          (set! row9_rec2 (append row9_rec2 (list number))))
                          (cons element next))
              (if(equal? numSoFar 75)
                          (begin
                          (set! row9_rec3 (append row9_rec3 (list number))))
                          (cons element next))
               (if(equal? numSoFar 76)
                          (begin
                          (set! row9_rec4 (append row9_rec4 (list number))))
                          (cons element next))
               (if(equal? numSoFar 77)
                          (begin
                          (set! row9_rec5 (append row9_rec5 (list number))))
                          (cons element next))
                (if(equal? numSoFar 78)
                          (begin
                          (set! row9_rec6 (append row9_rec6 (list number))))
                           (cons element next)) 
                (if(equal? numSoFar 79)
                          (begin
                          (set! row9_rec7 (append row9_rec7 (list number))))
                          (cons element next))                                     
                (if(equal? numSoFar 80)
                          (begin
                          (set! row9_rec8 (append row9_rec8 (list number))))
                          (cons element next))
                (if(equal? numSoFar 81)
                          (begin
                          (set! row9_rec9 (append row9_rec9 (list number))))
                         (cons element next)) 
               next)))
         empty lst))

(define (RemoveAndAppendFirmElementToRow1 numSoFar number lst)
    (foldl (lambda (element next)
          (cond ((list? element)
              (if(equal? numSoFar 1)
                 (begin
                   (set! newelement (append newelement (list (remove number element))))
                   (set! row1_rec1 (append row1_rec1 newelement))
                   (set! newelement '()))
                  (cons element next))
              (if(equal? numSoFar 2)
                 (begin
                   (set! newelement (append newelement (list (remove number element))))
                   (set! row1_rec2 (append row1_rec2 newelement))
                   (set! newelement '()))
                   (cons element next))
              (if(equal? numSoFar 3)
                   (begin
                   (set! newelement (append newelement (list (remove number element))))
                   (set! row1_rec3 (append row1_rec3 newelement))
                   (set! newelement '()))
                   (cons element next))
               (if(equal? numSoFar 4)
                   (begin
                   (set! newelement (append newelement (list (remove number element))))
                   (set! row1_rec4 (append row1_rec4 newelement))
                   (set! newelement '()))
                   (cons element next))
               (if(equal? numSoFar 5)
                   (begin
                   (set! newelement (append newelement (list (remove number element))))
                   (set! row1_rec5 (append row1_rec5 newelement))
                   (set! newelement '()))
                   (cons element next))
                (if(equal? numSoFar 6)
                   (begin
                   (set! newelement (append newelement (list (remove number element))))
                   (set! row1_rec6 (append row1_rec6 newelement))
                   (set! newelement '()))
                   (cons element next))           
               (if(equal? numSoFar 7)
                   (begin
                   (set! newelement (append newelement (list (remove number element))))
                   (set! row1_rec7 (append row1_rec7 newelement))
                   (set! newelement '()))
                   (cons element next))
               (if(equal? numSoFar 8)
                   (begin
                   (set! newelement (append newelement (list (remove number element))))
                   (set! row1_rec8 (append row1_rec8 newelement))
                   (set! newelement '()))
                   (cons element next))
               (if(equal? numSoFar 9)
                   (begin
                   (set! newelement (append newelement (list (remove number element))))
                   (set! row1_rec9 (append row1_rec9 newelement))
                   (set! newelement '()))
                   (cons element next))         

               next)))
         empty lst))


(define (RemoveAndAppendFirmElementToRow2 numSoFar number lst)
    (foldl (lambda (element next)
          (cond ((list? element)
              (if(equal? numSoFar 10)
                 (begin
                   (set! newelement (append newelement (list (remove number element))))
                   (set! row2_rec1 (append row2_rec1 newelement))
                   (set! newelement '()))
                  (cons element next))
              (if(equal? numSoFar 11)
                 (begin
                   (set! newelement (append newelement (list (remove number element))))
                   (set! row2_rec2 (append row2_rec2 newelement))
                   (set! newelement '()))
                   (cons element next))
              (if(equal? numSoFar 12)
                   (begin
                   (set! newelement (append newelement (list (remove number element))))
                   (set! row2_rec3 (append row2_rec3 newelement))
                   (set! newelement '()))
                   (cons element next))
               (if(equal? numSoFar 13)
                   (begin
                   (set! newelement (append newelement (list (remove number element))))
                   (set! row2_rec4 (append row2_rec4 newelement))
                   (set! newelement '()))
                   (cons element next))
               (if(equal? numSoFar 14)
                   (begin
                   (set! newelement (append newelement (list (remove number element))))
                   (set! row2_rec5 (append row2_rec5 newelement))
                   (set! newelement '()))
                   (cons element next))
                (if(equal? numSoFar 15)
                   (begin
                   (set! newelement (append newelement (list (remove number element))))
                   (set! row2_rec6 (append row2_rec6 newelement))
                   (set! newelement '()))
                   (cons element next))           
               (if(equal? numSoFar 16)
                   (begin
                   (set! newelement (append newelement (list (remove number element))))
                   (set! row2_rec7 (append row2_rec7 newelement))
                   (set! newelement '()))
                   (cons element next))
               (if(equal? numSoFar 17)
                   (begin
                   (set! newelement (append newelement (list (remove number element))))
                   (set! row2_rec8 (append row2_rec8 newelement))
                   (set! newelement '()))
                   (cons element next))
               (if(equal? numSoFar 18)
                   (begin
                   (set! newelement (append newelement (list (remove number element))))
                   (set! row2_rec9 (append row2_rec9 newelement))
                   (set! newelement '()))
                   (cons element next))         
               next)))
         empty lst))



(define (RemoveAndAppendFirmElementToRow3 numSoFar number lst)
    (foldl (lambda (element next)
          (cond ((list? element)
              (if(equal? numSoFar 19)
                 (begin
                   (set! newelement (append newelement (list (remove number element))))
                   (set! row3_rec1 (append row3_rec1 newelement))
                   (set! newelement '()))
                  (cons element next))
              (if(equal? numSoFar 20)
                 (begin
                   (set! newelement (append newelement (list (remove number element))))
                   (set! row3_rec2 (append row3_rec2 newelement))
                   (set! newelement '()))
                   (cons element next))
              (if(equal? numSoFar 21)
                   (begin
                   (set! newelement (append newelement (list (remove number element))))
                   (set! row3_rec3 (append row3_rec3 newelement))
                   (set! newelement '()))
                   (cons element next))
               (if(equal? numSoFar 22)
                   (begin
                   (set! newelement (append newelement (list (remove number element))))
                   (set! row3_rec4 (append row3_rec4 newelement))
                   (set! newelement '()))
                   (cons element next))
               (if(equal? numSoFar 23)
                   (begin
                   (set! newelement (append newelement (list (remove number element))))
                   (set! row3_rec5 (append row3_rec5 newelement))
                   (set! newelement '()))
                   (cons element next))
                (if(equal? numSoFar 24)
                   (begin
                   (set! newelement (append newelement (list (remove number element))))
                   (set! row3_rec6 (append row3_rec6 newelement))
                   (set! newelement '()))
                   (cons element next))           
               (if(equal? numSoFar 25)
                   (begin
                   (set! newelement (append newelement (list (remove number element))))
                   (set! row3_rec7 (append row3_rec7 newelement))
                   (set! newelement '()))
                   (cons element next))
               (if(equal? numSoFar 26)
                   (begin
                   (set! newelement (append newelement (list (remove number element))))
                   (set! row3_rec8 (append row3_rec8 newelement))
                   (set! newelement '()))
                   (cons element next))
               (if(equal? numSoFar 27)
                   (begin
                   (set! newelement (append newelement (list (remove number element))))
                   (set! row3_rec9 (append row3_rec9 newelement))
                   (set! newelement '()))
                   (cons element next))         
               next)))
         empty lst))

(define (RemoveAndAppendFirmElementToRow4 numSoFar number lst)
    (foldl (lambda (element next)
          (cond ((list? element)
              (if(equal? numSoFar 28)
                 (begin
                   (set! newelement (append newelement (list (remove number element))))
                   (set! row4_rec1 (append row4_rec1 newelement))
                   (set! newelement '()))
                  (cons element next))
              (if(equal? numSoFar 29)
                 (begin
                   (set! newelement (append newelement (list (remove number element))))
                   (set! row4_rec2 (append row4_rec2 newelement))
                   (set! newelement '()))
                   (cons element next))
              (if(equal? numSoFar 30)
                   (begin
                   (set! newelement (append newelement (list (remove number element))))
                   (set! row4_rec3 (append row4_rec3 newelement))
                   (set! newelement '()))
                   (cons element next))
               (if(equal? numSoFar 31)
                   (begin
                   (set! newelement (append newelement (list (remove number element))))
                   (set! row4_rec4 (append row4_rec4 newelement))
                   (set! newelement '()))
                   (cons element next))
               (if(equal? numSoFar 32)
                   (begin
                   (set! newelement (append newelement (list (remove number element))))
                   (set! row4_rec5 (append row4_rec5 newelement))
                   (set! newelement '()))
                   (cons element next))
                (if(equal? numSoFar 33)
                   (begin
                   (set! newelement (append newelement (list (remove number element))))
                   (set! row4_rec6 (append row4_rec6 newelement))
                   (set! newelement '()))
                   (cons element next))           
               (if(equal? numSoFar 34)
                   (begin
                   (set! newelement (append newelement (list (remove number element))))
                   (set! row4_rec7 (append row4_rec7 newelement))
                   (set! newelement '()))
                   (cons element next))
               (if(equal? numSoFar 35)
                   (begin
                   (set! newelement (append newelement (list (remove number element))))
                   (set! row4_rec8 (append row4_rec8 newelement))
                   (set! newelement '()))
                   (cons element next))
               (if(equal? numSoFar 36)
                   (begin
                   (set! newelement (append newelement (list (remove number element))))
                   (set! row4_rec9 (append row4_rec9 newelement))
                   (set! newelement '()))
                   (cons element next))         
               next)))
         empty lst))


(define (RemoveAndAppendFirmElementToRow5 numSoFar number lst)
    (foldl (lambda (element next)
          (cond ((list? element)
              (if(equal? numSoFar 37)
                 (begin
                   (set! newelement (append newelement (list (remove number element))))
                   (set! row5_rec1 (append row5_rec1 newelement))
                   (set! newelement '()))
                  (cons element next))
              (if(equal? numSoFar 38)
                 (begin
                   (set! newelement (append newelement (list (remove number element))))
                   (set! row5_rec2 (append row5_rec2 newelement))
                   (set! newelement '()))
                   (cons element next))
              (if(equal? numSoFar 39)
                   (begin
                   (set! newelement (append newelement (list (remove number element))))
                   (set! row5_rec3 (append row5_rec3 newelement))
                   (set! newelement '()))
                   (cons element next))
               (if(equal? numSoFar 40)
                   (begin
                   (set! newelement (append newelement (list (remove number element))))
                   (set! row5_rec4 (append row5_rec4 newelement))
                   (set! newelement '()))
                   (cons element next))
               (if(equal? numSoFar 41)
                   (begin
                   (set! newelement (append newelement (list (remove number element))))
                   (set! row5_rec5 (append row5_rec5 newelement))
                   (set! newelement '()))
                   (cons element next))
                (if(equal? numSoFar 42)
                   (begin
                   (set! newelement (append newelement (list (remove number element))))
                   (set! row5_rec6 (append row5_rec6 newelement))
                   (set! newelement '()))
                   (cons element next))           
               (if(equal? numSoFar 43)
                   (begin
                   (set! newelement (append newelement (list (remove number element))))
                   (set! row5_rec7 (append row5_rec7 newelement))
                   (set! newelement '()))
                   (cons element next))
               (if(equal? numSoFar 44)
                   (begin
                   (set! newelement (append newelement (list (remove number element))))
                   (set! row5_rec8 (append row5_rec8 newelement))
                   (set! newelement '()))
                   (cons element next))
               (if(equal? numSoFar 45)
                   (begin
                   (set! newelement (append newelement (list (remove number element))))
                   (set! row5_rec9 (append row5_rec9 newelement))
                   (set! newelement '()))
                   (cons element next))         
               next)))
         empty lst))



(define (RemoveAndAppendFirmElementToRow6 numSoFar number lst)
    (foldl (lambda (element next)
          (cond ((list? element)
              (if(equal? numSoFar 46)
                 (begin
                   (set! newelement (append newelement (list (remove number element))))
                   (set! row6_rec1 (append row6_rec1 newelement))
                   (set! newelement '()))
                  (cons element next))
              (if(equal? numSoFar 47)
                 (begin
                   (set! newelement (append newelement (list (remove number element))))
                   (set! row6_rec2 (append row6_rec2 newelement))
                   (set! newelement '()))
                   (cons element next))
              (if(equal? numSoFar 48)
                   (begin
                   (set! newelement (append newelement (list (remove number element))))
                   (set! row6_rec3 (append row6_rec3 newelement))
                   (set! newelement '()))
                   (cons element next))
               (if(equal? numSoFar 49)
                   (begin
                   (set! newelement (append newelement (list (remove number element))))
                   (set! row6_rec4 (append row6_rec4 newelement))
                   (set! newelement '()))
                   (cons element next))
               (if(equal? numSoFar 50)
                   (begin
                   (set! newelement (append newelement (list (remove number element))))
                   (set! row6_rec5 (append row6_rec5 newelement))
                   (set! newelement '()))
                   (cons element next))
                (if(equal? numSoFar 51)
                   (begin
                   (set! newelement (append newelement (list (remove number element))))
                   (set! row6_rec6 (append row6_rec6 newelement))
                   (set! newelement '()))
                   (cons element next))           
               (if(equal? numSoFar 52)
                   (begin
                   (set! newelement (append newelement (list (remove number element))))
                   (set! row6_rec7 (append row6_rec7 newelement))
                   (set! newelement '()))
                   (cons element next))
               (if(equal? numSoFar 53)
                   (begin
                   (set! newelement (append newelement (list (remove number element))))
                   (set! row6_rec8 (append row6_rec8 newelement))
                   (set! newelement '()))
                   (cons element next))
               (if(equal? numSoFar 54)
                   (begin
                   (set! newelement (append newelement (list (remove number element))))
                   (set! row6_rec9 (append row6_rec9 newelement))
                   (set! newelement '()))
                   (cons element next))         
               next)))
         empty lst))



(define (RemoveAndAppendFirmElementToRow7 numSoFar number lst)
    (foldl (lambda (element next)
          (cond ((list? element)
              (if(equal? numSoFar 55)
                 (begin
                   (set! newelement (append newelement (list (remove number element))))
                   (set! row7_rec1 (append row7_rec1 newelement))
                   (set! newelement '()))
                  (cons element next))
              (if(equal? numSoFar 56)
                 (begin
                   (set! newelement (append newelement (list (remove number element))))
                   (set! row7_rec2 (append row7_rec2 newelement))
                   (set! newelement '()))
                   (cons element next))
              (if(equal? numSoFar 57)
                   (begin
                   (set! newelement (append newelement (list (remove number element))))
                   (set! row7_rec3 (append row7_rec3 newelement))
                   (set! newelement '()))
                   (cons element next))
               (if(equal? numSoFar 58)
                   (begin
                   (set! newelement (append newelement (list (remove number element))))
                   (set! row7_rec4 (append row7_rec4 newelement))
                   (set! newelement '()))
                   (cons element next))
               (if(equal? numSoFar 59)
                   (begin
                   (set! newelement (append newelement (list (remove number element))))
                   (set! row7_rec5 (append row7_rec5 newelement))
                   (set! newelement '()))
                   (cons element next))
                (if(equal? numSoFar 60)
                   (begin
                   (set! newelement (append newelement (list (remove number element))))
                   (set! row7_rec6 (append row7_rec6 newelement))
                   (set! newelement '()))
                   (cons element next))           
               (if(equal? numSoFar 61)
                   (begin
                   (set! newelement (append newelement (list (remove number element))))
                   (set! row7_rec7 (append row7_rec7 newelement))
                   (set! newelement '()))
                   (cons element next))
               (if(equal? numSoFar 62)
                   (begin
                   (set! newelement (append newelement (list (remove number element))))
                   (set! row7_rec8 (append row7_rec8 newelement))
                   (set! newelement '()))
                   (cons element next))
               (if(equal? numSoFar 63)
                   (begin
                   (set! newelement (append newelement (list (remove number element))))
                   (set! row7_rec9 (append row7_rec9 newelement))
                   (set! newelement '()))
                   (cons element next))         
               next)))
         empty lst))

(define (RemoveAndAppendFirmElementToRow8 numSoFar number lst)
    (foldl (lambda (element next)
          (cond ((list? element)
              (if(equal? numSoFar 64)
                 (begin
                   (set! newelement (append newelement (list (remove number element))))
                   (set! row8_rec1 (append row8_rec1 newelement))
                   (set! newelement '()))
                  (cons element next))
              (if(equal? numSoFar 65)
                 (begin
                   (set! newelement (append newelement (list (remove number element))))
                   (set! row8_rec2 (append row8_rec2 newelement))
                   (set! newelement '()))
                   (cons element next))
              (if(equal? numSoFar 66)
                   (begin
                   (set! newelement (append newelement (list (remove number element))))
                   (set! row8_rec3 (append row8_rec3 newelement))
                   (set! newelement '()))
                   (cons element next))
               (if(equal? numSoFar 67)
                   (begin
                   (set! newelement (append newelement (list (remove number element))))
                   (set! row8_rec4 (append row8_rec4 newelement))
                   (set! newelement '()))
                   (cons element next))
               (if(equal? numSoFar 68)
                   (begin
                   (set! newelement (append newelement (list (remove number element))))
                   (set! row8_rec5 (append row8_rec5 newelement))
                   (set! newelement '()))
                   (cons element next))
                (if(equal? numSoFar 69)
                   (begin
                   (set! newelement (append newelement (list (remove number element))))
                   (set! row8_rec6 (append row8_rec6 newelement))
                   (set! newelement '()))
                   (cons element next))           
               (if(equal? numSoFar 70)
                   (begin
                   (set! newelement (append newelement (list (remove number element))))
                   (set! row8_rec7 (append row8_rec7 newelement))
                   (set! newelement '()))
                   (cons element next))
               (if(equal? numSoFar 71)
                   (begin
                   (set! newelement (append newelement (list (remove number element))))
                   (set! row8_rec8 (append row8_rec8 newelement))
                   (set! newelement '()))
                   (cons element next))
               (if(equal? numSoFar 72)
                   (begin
                   (set! newelement (append newelement (list (remove number element))))
                   (set! row8_rec9 (append row8_rec9 newelement))
                   (set! newelement '()))
                   (cons element next))         
               next)))
         empty lst))

(define (RemoveAndAppendFirmElementToRow9 numSoFar number lst)
    (foldl (lambda (element next)
          (cond ((list? element)
              (if(equal? numSoFar 73)
                 (begin
                   (set! newelement (append newelement (list (remove number element))))
                   (set! row9_rec1 (append row9_rec1 newelement))
                   (set! newelement '()))
                  (cons element next))
              (if(equal? numSoFar 74)
                 (begin
                   (set! newelement (append newelement (list (remove number element))))
                   (set! row9_rec2 (append row9_rec2 newelement))
                   (set! newelement '()))
                   (cons element next))
              (if(equal? numSoFar 75)
                   (begin
                   (set! newelement (append newelement (list (remove number element))))
                   (set! row9_rec3 (append row9_rec3 newelement))
                   (set! newelement '()))
                   (cons element next))
               (if(equal? numSoFar 76)
                   (begin
                   (set! newelement (append newelement (list (remove number element))))
                   (set! row9_rec4 (append row9_rec4 newelement))
                   (set! newelement '()))
                   (cons element next))
               (if(equal? numSoFar 77)
                   (begin
                   (set! newelement (append newelement (list (remove number element))))
                   (set! row9_rec5 (append row9_rec5 newelement))
                   (set! newelement '()))
                   (cons element next))
                (if(equal? numSoFar 78)
                   (begin
                   (set! newelement (append newelement (list (remove number element))))
                   (set! row9_rec6 (append row9_rec6 newelement))
                   (set! newelement '()))
                   (cons element next))           
               (if(equal? numSoFar 79)
                   (begin
                   (set! newelement (append newelement (list (remove number element))))
                   (set! row9_rec7 (append row9_rec7 newelement))
                   (set! newelement '()))
                   (cons element next))
               (if(equal? numSoFar 80)
                   (begin
                   (set! newelement (append newelement (list (remove number element))))
                   (set! row9_rec8 (append row9_rec8 newelement))
                   (set! newelement '()))
                   (cons element next))
               (if(equal? numSoFar 81)
                   (begin
                   (set! newelement (append newelement (list (remove number element))))
                   (set! row9_rec9 (append row9_rec9 newelement))
                   (set! newelement '()))
                   (cons element next))         
               next)))
         empty lst))

(define (removeFromColumn1 numSoFar number lst)
    (foldl (lambda (element next)
          (cond ((list? element)
                (if(equal? 1 (length element))
                   (set! column1_rec1  (append column1_rec1 element))
                    (cons element next))
              (if(not(equal? 1 (length element)))
                     (set! column1_rec1 (remove number element))
                    (cons element next))
               next)))
         empty lst))


(define (removeFromBox1 numSoFar number lst)
    (foldl (lambda (element next)
          (cond ((list? element)
                (if(equal? 1 (length element))
                   (set! box1_rec1  (append box1_rec1 element))
                    (cons element next))
              (if(not(equal? 1 (length element)))
                     (set! box1_rec1 (remove number element))
                    (cons element next))
               next)))
         empty lst))


(define (transform lst )
  (define (createSets lst)
    (foldl (lambda (element next)
          (cond ((number? element)
               (if (equal? 0 element)
                   (set! PossibleMatrix (append PossibleMatrix (list  (range 1 10))))
                    (cons element next))
                (if (not(equal? 0 element))
                   (set! PossibleMatrix (append PossibleMatrix (list (list element))))
                    (cons element next))
               next)))
         empty lst)
 (addToList PossibleMatrix))
     (define (firstList) (first lst))
     (define (secondList) (second lst))
     (define (thirdList) (third lst))
     (define (fourthList) (fourth lst))
     (define (fifthList) (fifth lst))
     (define (sixthList) (sixth lst))
     (define (seventhList) (seventh lst))
     (define (eighthList) (eighth lst))
     (define (ninthList) (ninth lst))
     (list (createSets (firstList))
           (createSets (secondList))
           (createSets (thirdList))
           (createSets (fourthList))
           (createSets (fifthList))
           (createSets (sixthList))
           (createSets (seventhList))
           (createSets (eighthList))
           (createSets (ninthList))))

(transform Matrix)

;row1
(solve PossibleMatrix)
;mergeAllRow3
;row1_rec1
;row1_rec2
;row1_rec3
;row1_rec4
;row1_rec5
;row1_rec6
;row1_rec7
;row1_rec8
;row1_rec9
;(set! row1_rec1 (append (remove 1  (list 1 2 3 4 5 6 7 8 9))))
;row1_rec1
;(set! row1_rec1 (append (remove 2  (list 1 2 3 4 5 6 7 8 9))))
