#lang racket
(define (SolveSudoku sentboard)
  (define (subgrid brd r c)
    (define ll '((0 1 2)(3 4 5)(6 7 8)))
    (define-values (rr cc)
      (values (flatten (filter (λ (x) (member r x)) ll))
              (flatten (filter (λ (x) (member c x)) ll))))
    (for*/list ((i rr)(j cc))
      (list-ref (list-ref brd i) j)))
  (let loop ((bd sentboard) (r 0) (c 0))
    (cond [(= 0 (list-ref (list-ref bd r) c) )
           (for ((i (range 1 10)))
             (when (and (not(member i (list-ref bd r)))                 
                        (not(member i (map (λ (x) (list-ref x c)) bd))) 
                        (not(member i (subgrid bd r c))))                
               (define newbd (list-set bd r
                                       (list-set (list-ref bd r) c i)))
               (cond [(< (add1 c) 9)
                      (loop newbd r (add1 c) )]
                     [(< (add1 r) 9)
                      (loop newbd (add1 r) 0 )]
                     [else (displayln "SOLUTION:")
                           (for ((rowline newbd)) (println rowline))])))]
          [(< (add1 c) 9)
           (loop bd r (add1 c))]
          [(< (add1 r) 9)
           (loop bd (add1 r) 0)]
          [else (displayln "Solution:")  
                (for ((rowline bd)) (println rowline))])))



(define board                   
  '((0 0 3 0 2 0 6 0 0)
    (9 0 0 3 0 5 0 0 1)
    (0 0 1 8 0 6 4 0 0)
    (0 0 8 1 0 2 9 0 0)
    (7 0 0 0 0 0 0 0 8)
    (0 0 6 7 0 8 2 0 0)
    (0 0 2 6 0 9 5 0 0)
    (8 0 0 2 0 3 0 0 9)
    (0 0 5 0 1 0 3 0 0)))

(define b2
  '((3 9 4 0 0 2 6 7 0)
    (0 0 0 3 0 0 4 0 0)
    (5 0 0 6 9 0 0 2 0)
    (0 4 5 0 0 0 9 0 0)
    (6 0 0 0 0 0 0 0 7)
    (0 0 7 0 0 0 5 8 0)
    (0 1 0 0 6 7 0 0 8)
    (0 0 9 0 0 8 0 0 0)
    (0 2 6 4 0 0 7 3 5)))

(SolveSudoku board)
(SolveSudoku b2)